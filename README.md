# README #

This repository holds a collection of R tools for performing
various tasks such as scraping web pages, loading xml files
from URL, calculating word frequency from a text, plotting
word frequency on a bar graph, etc.  It is intended to serve
as toolkit for Digital Humanists interesting in performing
various types of textual analysis.

The tools were initially developed as course material for
a Rare Books School (https://rarebookschool.org/) course 
on Digital Approaches to Bibliography and Book History 
co-taught by Carl Stahmer and Benjamin Pauley.

Documentation can be found at the repository Wiki at
https://bitbucket.org/cstahmer/r-text-tools/wiki/Home.

For more information please contact Carl Stahmer via the 
Contact link on his website http://www.carlstahmer.com.
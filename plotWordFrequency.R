#######################################################
# Takes a text blob and plots frequency               #
#                                                     #
# Digital Approaches to Bibliography and Book History #
#                                                     #
# Rare Book School - University of Virginia           #
#                                                     #
# Carl Stahmer and Benjamin Pauley                    #
#                                                     #
#######################################################
# USAGE:                                              #
#                                                     #
# text <- the text string you want to calculate       #
# exclude <- a comma separated list of words you want #
#           to exclude from the count and plot        #
# maxnum <- the maximum number of words to plot       #
#          starting from the highest frequency and    #
#          counting down                              #
#                                                     #
# plotWordFrequency(text, exclude, maxnum)            #
#                                                     #
# examples:                                           #
#                                                     #
# plotWordFrequency(text, "cat,dog", 5)               #
# plotWordFrequency(text, "cat,dog")                  #  
# plotWordFrequency(text, "", 5)                      #
# plotWordFrequency(text)                             #
#                                                     #
#######################################################
#                                                     #
# This work is licensed under a Creative Commons      #
# Attribution 4.0 International License.              #
#                                                     #
#######################################################

plotWordFrequency <- function(input, stopadds="", maxnumber=0, ...) {
  ###---PACKAGES ---###
  
  # create a words vector
  text.base<-tolower(input)
  text.words.list<-strsplit(text.base, "\\W+", perl=TRUE)
  text.words.vector<-unlist(text.words.list)
  
  # remove stop words
  stopWords = c("a","an","and","are","as","at","be","by","for","from","has","he","in","is","it","its","of","on","that","the","to","was","were","will","with")
  if (nchar(stopadds) > 0) {
    newstops <- unlist(strsplit(stopadds, ","))
    for (thisstopword in newstops) {
      stopWords = c(stopWords, thisstopword)
    }
  }
  for (word in stopWords) {
    text.words.vector <- text.words.vector[text.words.vector != word]
  }
  
  # make the frequency list
  text.freq.list<-table(text.words.vector)  
  text.sorted.freq.list<-sort(text.freq.list, decreasing=TRUE)
  
  # set number to plot
  if (maxnumber != 0 && maxnumber < length(text.sorted.freq.list)) {
    length(text.sorted.freq.list) <- maxnumber
  }
  
  barplot(text.sorted.freq.list, main = "Word Frequency")
  
  return(1)
}
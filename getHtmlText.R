#######################################################
# Extract the text from an html page                  #
#                                                     #
# Digital Approaches to Bibliography and Book History #
#                                                     #
# Rare Book School - University of Virginia           #
#                                                     #
# Carl Stahmer and Benjamin Pauley                    #
#######################################################
# USAGE:                                              #
#                                                     #
# input <- "http://www.carlstahmer.com"               # 
#                                                     #
# r> txt <- getHtmlText(input)                        #
# r> txt                                              #      
#                                                     #
#######################################################
#                                                     #
# This work is licensed under a Creative Commons      #
# Attribution 4.0 International License.              #
#                                                     #
#######################################################

getHtmlText <- function(input, save=0, sfile="r-text.txt", ...) {
  ###---PACKAGES ---###
  require(RCurl)
  require(XML)
  
  
  ###--- LOCAL FUNCTIONS ---###
  # Determine how to grab html for a single input element
  evaluate_input <- function(input) {    
    # if input is a .html file
    if(file.exists(input)) {
      char.vec <- readLines(input, warn = FALSE)
      return(paste(char.vec, collapse = ""))
    }
    
    # if input is html text
    if(grepl("</html>", input, fixed = TRUE)) return(input)
    
    # if input is a URL, probably should use a regex here instead?
    if(!grepl(" ", input)) {
      # downolad SSL certificate in case of https problem
      if(!file.exists("cacert.perm")) download.file(url="http://curl.haxx.se/ca/cacert.pem", destfile="cacert.perm")
      return(getURL(input, followlocation = TRUE, cainfo = "cacert.perm"))
    }
    
    # return NULL if none of the conditions above apply
    return(NULL)
  }
  
  # convert HTML to plain text
  convert_html_to_text <- function(html) {
    doc <- htmlParse(html, asText = TRUE)
    text <- xpathSApply(doc, "//text()[not(ancestor::script)][not(ancestor::style)][not(ancestor::noscript)][not(ancestor::form)]", xmlValue)
    text <- gsub("[\r\n\t]", "", text)
    text <- gsub("[\\\"]", "'", text)
    text <- gsub('\\s+', ' ',text)
    
    return(text)
  }
  
  # format text vector into one character string
  collapse_text <- function(txt) {
    return(paste(txt, collapse = " "))
  }
  
  ###--- MAIN ---###
  # STEP 1: Evaluate input
  html.list <- lapply(input, evaluate_input)
  
  # STEP 2: Extract text from HTML
  text.list <- lapply(html.list, convert_html_to_text)
  
  # STEP 3: Return text
  text.vector <- sapply(text.list, collapse_text)
  
  if (save > 0) {
    for (idx in text.vector) {
      thisline <- paste(idx)
      write(thisline, file = sfile, append = TRUE) 
    }
  }
  return(text.vector)
}